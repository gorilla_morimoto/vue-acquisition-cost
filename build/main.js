// @ts-check

new Vue({
  el: '#app',
  data: {
    // デバッグ中に毎回入力するのは面倒だったので決め打ちしています
    // 変数名が微妙なのは認めます
    h: '40', // holdings
    ap: '2033', // acquisition price
    cp: '2074', // current price
    pa: '' // planned amount
  },
  computed: {
    newAP() {
      // NaNの場合は何も表示させたくないので文字列も埋め込みます
      const ans = this.calcAP();
      return isNaN(ans) ? '' : `新しい取得単価: ${ans}`;
    },
    newProfit() {
      // NaNの場合は何も表示させたくないので文字列も埋め込みます
      const profit = this.calcProfit();
      return isNaN(profit) ? '' : `評価損益: ${profit}`;
    },
    currentProfit() {
      return (this.cp - this.ap) * this.h;
    },
    isValid() {
      // @ts-ignore
      return this.newAP !== '';
    }
  },
  methods: {
    calcAP() {
      // 以下の値をcomputedとして持っておこうかと思いましたが、
      // そうするとcomputedとmethodが相互依存してしまいます
      const h = parseInt(this.h);
      const ap = parseInt(this.ap);
      const cp = parseInt(this.cp);
      const pa = parseInt(this.pa);
      return (ap * h + cp * pa) / (h + pa);
    },
    calcProfit() {
      const newAP = this.calcAP();
      const h = parseInt(this.h);
      const cp = parseInt(this.cp);
      const pa = parseInt(this.pa);
      return (cp - newAP) * (h + pa);
    },
    reflect() {
      if (this.newAP !== '') {
        // apは現在の値を元に計算する項目なので
        // ほかの項目を更新する前にいじる必要がある
        this.ap = Math.floor(this.calcAP());
        this.h = parseInt(this.h) + parseInt(this.pa);
      }
    }
  }
});
