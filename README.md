# 株の平均取得単価を計算する

2018-05-20

Vue.jsを使って株の取得単価を計算するアプリケーション

![screenshot](assets/screen.png)

## TODO

- [ ] デザインの改善
- [x] 画面のスクリーンショットをREADMEに
- [x] 現在の評価損益を表示
- [x] 現在のシミュレーション結果をインプットに反映する

